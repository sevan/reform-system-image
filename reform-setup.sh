# This script launches the Setup Wizard on first boot and auto-login as root.
#
if [ "$(whoami)" = "root" ] && [ "$(tty)" = '/dev/tty1' ]
then
        clear
        echo "Starting MNT Reform Setup Wizard..."

        /usr/bin/sway --config /usr/share/reform-setup-wizard/reform-setup-sway-config
fi
