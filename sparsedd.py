#!/usr/bin/env python

import subprocess
import os
import sys


def main():
    if len(sys.argv) not in [5, 6]:
        print("usage: sparsedd infile outfile inoffset outoffset [length]")
        exit(1)
    infd = os.open(sys.argv[1], os.O_RDONLY)
    inlength = os.lseek(infd, 0, os.SEEK_END)
    outfd = os.open(sys.argv[2], os.O_CREAT | os.O_WRONLY)
    outlength = os.lseek(outfd, 0, os.SEEK_END)
    inoffset = int(sys.argv[3])
    outoffset = int(sys.argv[4])
    if len(sys.argv) > 5:
        inlength = inoffset + int(sys.argv[5])
    data_copied = 0
    curr = inoffset
    # curr: the current position in the input file
    # inoffset: the position at which to start reading from the input file
    # outoffset: the position at which to start writing to the output file
    # hole: the position in the input file where the next hole starts
    # data: the position in the input file where the next data starts
    # inlength: position in the input file until which to read from
    while True:
        try:
            data = os.lseek(infd, curr, os.SEEK_DATA)
        except OSError:
            # no more data
            break
        if data >= inlength:
            break
        try:
            hole = os.lseek(infd, data, os.SEEK_HOLE)
        except OSError:
            # no hole afterwards, copy until EOF
            hole = inlength
        # Copy either as many bytes as can be found from the next start of data
        # to the next start of a hole or,
        # copy as many bytes as from the current position until the position in
        # the input file until which to read from.
        count = min(inlength, hole) - data
        data_copied += count
        print(
            f"copying range {data}..{data+count} ({100*(data+count-inoffset)/(inlength-inoffset):.2f}%)",
            file=sys.stderr,
        )
        try:
            os.copy_file_range(
                infd,
                outfd,
                count,
                offset_src=data,
                offset_dst=data + outoffset - inoffset,
            )
        except OSError as e:
            import errno

            if e.errno == errno.EXDEV:
                print(
                    "E: input and output must be on the same filesystem or (if "
                    "not) are not of the same type"
                )
                exit(1)
            else:
                raise
        curr = hole
        if hole >= inlength:
            break
    if outlength - outoffset < inlength - inoffset:
        os.truncate(outfd, inlength - inoffset + outoffset)
    os.close(infd)
    os.close(outfd)
    print(
        f"copied {data_copied/1024/1024:.2f} MB {100*data_copied/(inlength - inoffset):.2f}%"
    )


if __name__ == "__main__":
    main()
