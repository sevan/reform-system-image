# MNT Reform System Image

This is a collection of scripts used to build the MNT Reform System Images, which are based on Debian GNU/Linux. The images can be transferred to an SD card and booted by an MNT Reform computer.

## Downloading and Flashing a System Image

Most users will want to download a pre-built image from: https://mnt.re/system-image

1. From the link above, download the file whose name matches the Processor Module in your MNT Reform:
  - NXP i.MX8MQ Processor: `reform-system-imx8mq.img.gz`
  - RCM4 with BPi CM4/A311D Processor: `reform-system-a311d.img.gz`
  - NXP LS1028A Processor: `reform-system-ls1028a.img.gz`

2. Unzip the image using `gunzip reform-system-imx8mq.img.gz` (Substitute your actual image file name, also in the following steps)

3. Identify the device name of your SD card. For example, you can check the output of `lsblk` before and after you insert the card and compare which new device appears. Below we use the device `sdx`, but it will probably be a different one on your computer.

4. Copy the image to your SD card using `sudo dd if=reform-system-imx8mq.img of=/dev/sdx bs=8M status=progress`.

## How to Build the Images

Building the images requires considerable disk space and CPU resources, it is generally only required if you want to contribute to the development of the system images.

To create a system image for all platforms, run this:

    ./mkimage.sh

To only create the image for a specific platform, choose one out of the
following list:

 - `pocket-reform-system-a311d`
 - `pocket-reform-system-imx8mp`
 - `reform-system-a311d`
 - `reform-system-imx8mp`
 - `reform-system-imx8mq`
 - `reform-system-ls1028a`
 - `reform-system-rk3588`

And pass it as the positional argument, for example for the MNT Reform 2 with
imx8mq, use:

    ./mkimage.sh reform-system-imx8mq

The resulting image can then be found as an *.img file in the current
directory.

## License

Copyright 2018-2020 Lukas F. Hartmann / MNT Research GmbH

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
